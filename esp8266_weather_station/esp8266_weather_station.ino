//Example of using ESP8266 and Weather Underground and ThingSpeak API with DHT22 Sensor and HD44780 4x20 LCD with I2C Bus
//***Created by marcinadd on 25 May 2018
//***Public released on 28 Aug 2018

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <Timer.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
#include "DHT.h"

//Change to DHT11 if you are using DHT11
#define DHTTYPE DHT22
//On NodeMCU/Wemos Boards it will be D3 pin
#define DHTPIN 2
//On NodeMCU/Wemos Boards it will be D4 pin
#define BUTTONPIN 0

void dht_wrapper();
DHT sensor(DHTPIN, DHTTYPE);

//---WiFi DATA---
const char* ssid = "YOUR_SSID_HERE";
const char* password = "YOUR_PASS_HERE";

//---WU DATA---
const char* wu_host = "api.wunderground.com";
const char* wu_key = "YOUR_WU_KEY_HERE";
const char* country = "YOUR_COUNTRY_CODE_HERE";
const char* city = "YOUR_CITY_HERE";

//---Thingspeak DATA---
const char* ts_key = "YOUR_TS_KEY_HERE";
const char* ts_host = "api.thingspeak.com";

//---Intervals---
//Update Data from WU and TS interval (in millis) (Default 10*60*1000) [Should be >=6*60*1000]
const int data_interval = 7 * 60 * 1000;
//Update Data from DHT interval (in millis) (Default 30*1000) [Must be >=2*1000]
const int dht_interval = 30 * 1000;
//Update Screen interval (Default 300)
const int screen_interval = 300;

//---Timers---
Timer dht;
Timer screen;
Timer data;

//---Global Variables---
//----Mode
//-----0-Current Conditions + DHT Reading
//-----1-Forecast
int mode = 1;
byte dayId = 1;
//Prevent screen blinking while using lcd.clear()
bool full_update = false;
bool update_forecast = false;

bool key_released = true;


//From DHT
float temperature = 0;
float humidity = 0;

//Current weather info
String currentTime;
String currentWeather;

//Struct for Forecast
struct forecastDay {
  String week_day;
  int date_day;
  String monthname;
  String conditions;
  String low_temp;
  String high_temp;
  int qpf_allday;
  int avewind_speed;
  String avewind_dir;
};

forecastDay forecast_days[4];

void parseCurrentConditions(String json) {
  DynamicJsonBuffer jsonBuffer;

  JsonObject& root = jsonBuffer.parseObject(json);

  JsonObject& current_observation = root["current_observation"];

  const char* current_observation_observation_time = current_observation["observation_time"];
  const char* current_observation_observation_time_rfc822 = current_observation["observation_time_rfc822"];
  const char* current_observation_local_time_rfc822 = current_observation["local_time_rfc822"];
  const char* current_observation_weather = current_observation["weather"];

  currentTime = current_observation_local_time_rfc822;
  currentTime = cutString(currentTime, 5, 25);
  currentWeather = current_observation_weather;
  currentWeather = cutString(current_observation_weather, 0, 19);
}

void parseForecast(String json) {
  DynamicJsonBuffer jsonBuffer;

  JsonObject& root = jsonBuffer.parseObject(json);

  JsonArray& forecast_simpleforecast_forecastday = root["forecast"]["simpleforecast"]["forecastday"];
  //---0---
  //Not used currently
  JsonObject& forecast_simpleforecast_forecastday0 = forecast_simpleforecast_forecastday[0];
  JsonObject& forecast_simpleforecast_forecastday0_date = forecast_simpleforecast_forecastday0["date"];
  int forecast_simpleforecast_forecastday0_date_day = forecast_simpleforecast_forecastday0_date["day"];
  const char* forecast_simpleforecast_forecastday0_date_monthname_short = forecast_simpleforecast_forecastday0_date["monthname_short"];
  const char* forecast_simpleforecast_forecastday0_date_weekday_short = forecast_simpleforecast_forecastday0_date["weekday_short"];
  const char* forecast_simpleforecast_forecastday0_high_celsius = forecast_simpleforecast_forecastday0["high"]["celsius"];
  const char* forecast_simpleforecast_forecastday0_low_celsius = forecast_simpleforecast_forecastday0["low"]["celsius"];
  const char* forecast_simpleforecast_forecastday0_conditions = forecast_simpleforecast_forecastday0["conditions"];
  //QPF- Quantitative Precipitation Forecasts
  int forecast_simpleforecast_forecastday0_qpf_allday_mm = forecast_simpleforecast_forecastday0["qpf_allday"]["mm"];
  JsonObject& forecast_simpleforecast_forecastday0_avewind = forecast_simpleforecast_forecastday0["avewind"];
  int forecast_simpleforecast_forecastday0_avewind_kph = forecast_simpleforecast_forecastday0_avewind["kph"];
  const char* forecast_simpleforecast_forecastday0_avewind_dir = forecast_simpleforecast_forecastday0_avewind["dir"];
  //---1---
  JsonObject& forecast_simpleforecast_forecastday1 = forecast_simpleforecast_forecastday[1];
  JsonObject& forecast_simpleforecast_forecastday1_date = forecast_simpleforecast_forecastday1["date"];
  int forecast_simpleforecast_forecastday1_date_day = forecast_simpleforecast_forecastday1_date["day"];
  const char* forecast_simpleforecast_forecastday1_date_monthname_short = forecast_simpleforecast_forecastday1_date["monthname_short"];
  const char* forecast_simpleforecast_forecastday1_date_weekday_short = forecast_simpleforecast_forecastday1_date["weekday_short"];
  const char* forecast_simpleforecast_forecastday1_high_celsius = forecast_simpleforecast_forecastday1["high"]["celsius"];
  const char* forecast_simpleforecast_forecastday1_low_celsius = forecast_simpleforecast_forecastday1["low"]["celsius"];
  const char* forecast_simpleforecast_forecastday1_conditions = forecast_simpleforecast_forecastday1["conditions"];
  int forecast_simpleforecast_forecastday1_qpf_allday_mm = forecast_simpleforecast_forecastday1["qpf_allday"]["mm"];
  JsonObject& forecast_simpleforecast_forecastday1_avewind = forecast_simpleforecast_forecastday1["avewind"];
  int forecast_simpleforecast_forecastday1_avewind_kph = forecast_simpleforecast_forecastday1_avewind["kph"];
  const char* forecast_simpleforecast_forecastday1_avewind_dir = forecast_simpleforecast_forecastday1_avewind["dir"];
  //---2---
  JsonObject& forecast_simpleforecast_forecastday2 = forecast_simpleforecast_forecastday[2];
  JsonObject& forecast_simpleforecast_forecastday2_date = forecast_simpleforecast_forecastday2["date"];
  int forecast_simpleforecast_forecastday2_date_day = forecast_simpleforecast_forecastday2_date["day"];
  const char* forecast_simpleforecast_forecastday2_date_monthname_short = forecast_simpleforecast_forecastday2_date["monthname_short"];
  const char* forecast_simpleforecast_forecastday2_date_weekday_short = forecast_simpleforecast_forecastday2_date["weekday_short"];
  const char* forecast_simpleforecast_forecastday2_high_celsius = forecast_simpleforecast_forecastday2["high"]["celsius"];
  const char* forecast_simpleforecast_forecastday2_low_celsius = forecast_simpleforecast_forecastday2["low"]["celsius"];
  const char* forecast_simpleforecast_forecastday2_conditions = forecast_simpleforecast_forecastday2["conditions"];
  int forecast_simpleforecast_forecastday2_qpf_allday_mm = forecast_simpleforecast_forecastday2["qpf_allday"]["mm"];
  JsonObject& forecast_simpleforecast_forecastday2_avewind = forecast_simpleforecast_forecastday2["avewind"];
  int forecast_simpleforecast_forecastday2_avewind_kph = forecast_simpleforecast_forecastday2_avewind["kph"];
  const char* forecast_simpleforecast_forecastday2_avewind_dir = forecast_simpleforecast_forecastday2_avewind["dir"];
  //---3---
  JsonObject& forecast_simpleforecast_forecastday3 = forecast_simpleforecast_forecastday[3];
  JsonObject& forecast_simpleforecast_forecastday3_date = forecast_simpleforecast_forecastday3["date"];
  int forecast_simpleforecast_forecastday3_date_day = forecast_simpleforecast_forecastday3_date["day"];
  const char* forecast_simpleforecast_forecastday3_date_monthname_short = forecast_simpleforecast_forecastday3_date["monthname_short"];
  const char* forecast_simpleforecast_forecastday3_date_weekday_short = forecast_simpleforecast_forecastday3_date["weekday_short"];
  const char* forecast_simpleforecast_forecastday3_high_celsius = forecast_simpleforecast_forecastday3["high"]["celsius"];
  const char* forecast_simpleforecast_forecastday3_low_celsius = forecast_simpleforecast_forecastday3["low"]["celsius"];
  const char* forecast_simpleforecast_forecastday3_conditions = forecast_simpleforecast_forecastday3["conditions"];
  int forecast_simpleforecast_forecastday3_qpf_allday_mm = forecast_simpleforecast_forecastday3["qpf_allday"]["mm"];
  JsonObject& forecast_simpleforecast_forecastday3_avewind = forecast_simpleforecast_forecastday3["avewind"];
  int forecast_simpleforecast_forecastday3_avewind_kph = forecast_simpleforecast_forecastday3_avewind["kph"];
  const char* forecast_simpleforecast_forecastday3_avewind_dir = forecast_simpleforecast_forecastday3_avewind["dir"];

  //TODO Add loop here
  forecast_days[0].week_day = forecast_simpleforecast_forecastday0_date_weekday_short;
  forecast_days[0].date_day = forecast_simpleforecast_forecastday0_date_day;
  forecast_days[0].monthname = forecast_simpleforecast_forecastday0_date_monthname_short;
  forecast_days[0].conditions = forecast_simpleforecast_forecastday0_conditions;
  forecast_days[0].low_temp = forecast_simpleforecast_forecastday0_low_celsius;
  forecast_days[0].high_temp = forecast_simpleforecast_forecastday0_high_celsius;
  forecast_days[0].qpf_allday = forecast_simpleforecast_forecastday0_qpf_allday_mm;
  forecast_days[0].avewind_speed = forecast_simpleforecast_forecastday0_avewind_kph;
  forecast_days[0].avewind_dir = forecast_simpleforecast_forecastday0_avewind_dir;

  forecast_days[1].week_day = forecast_simpleforecast_forecastday1_date_weekday_short;
  forecast_days[1].date_day = forecast_simpleforecast_forecastday1_date_day;
  forecast_days[1].monthname = forecast_simpleforecast_forecastday1_date_monthname_short;
  forecast_days[1].conditions = forecast_simpleforecast_forecastday1_conditions;
  forecast_days[1].low_temp = forecast_simpleforecast_forecastday1_low_celsius;
  forecast_days[1].high_temp = forecast_simpleforecast_forecastday1_high_celsius;
  forecast_days[1].qpf_allday = forecast_simpleforecast_forecastday1_qpf_allday_mm;
  forecast_days[1].avewind_speed = forecast_simpleforecast_forecastday1_avewind_kph;
  forecast_days[1].avewind_dir = forecast_simpleforecast_forecastday1_avewind_dir;

  forecast_days[2].week_day = forecast_simpleforecast_forecastday2_date_weekday_short;
  forecast_days[2].date_day = forecast_simpleforecast_forecastday2_date_day;
  forecast_days[2].monthname = forecast_simpleforecast_forecastday2_date_monthname_short;
  forecast_days[2].conditions = forecast_simpleforecast_forecastday2_conditions;
  forecast_days[2].low_temp = forecast_simpleforecast_forecastday2_low_celsius;
  forecast_days[2].high_temp = forecast_simpleforecast_forecastday2_high_celsius;
  forecast_days[2].qpf_allday = forecast_simpleforecast_forecastday2_qpf_allday_mm;
  forecast_days[2].avewind_speed = forecast_simpleforecast_forecastday2_avewind_kph;
  forecast_days[2].avewind_dir = forecast_simpleforecast_forecastday2_avewind_dir;

  forecast_days[3].week_day = forecast_simpleforecast_forecastday3_date_weekday_short;
  forecast_days[3].date_day = forecast_simpleforecast_forecastday3_date_day;
  forecast_days[3].monthname = forecast_simpleforecast_forecastday3_date_monthname_short;
  forecast_days[3].conditions = forecast_simpleforecast_forecastday3_conditions;
  forecast_days[3].low_temp = forecast_simpleforecast_forecastday3_low_celsius;
  forecast_days[3].high_temp = forecast_simpleforecast_forecastday3_high_celsius;
  forecast_days[3].qpf_allday = forecast_simpleforecast_forecastday3_qpf_allday_mm;
  forecast_days[3].avewind_speed = forecast_simpleforecast_forecastday3_avewind_kph;
  forecast_days[3].avewind_dir = forecast_simpleforecast_forecastday3_avewind_dir;

  for (int i = 0; i <= 3; i++) {
    forecast_days[i].conditions = cutString(forecast_days[i].conditions, 0, 19);
    forecast_days[i].avewind_dir = cutString(forecast_days[i].avewind_dir, 0, 9);
  }

}

void getCurrentConditions() {
  Serial.println("Getting current conditions...");
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    String url = "http://";
    url += wu_host;
    url += "/api/";
    url += wu_key;
    url += "/conditions/q/";
    url += country;
    url += "/";
    url += city;
    url += ".json";
    http.begin(url);
    digitalWrite(14, HIGH);
    int httpCode = http.GET();
    if (httpCode > 0) {
      parseCurrentConditions(http.getString());
      full_update = true;
    }
    http.end();
    digitalWrite(14, LOW);
  }

}


void getForecast() {
  Serial.println("Getting forecast...");
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    String url = "http://";
    url += wu_host;
    url += "/api/";
    url += wu_key;
    url += "/forecast/q/";
    url += country;
    url += "/";
    url += city;
    url += ".json";
    http.begin(url);
    digitalWrite(14, HIGH);
    int httpCode = http.GET();
    Serial.println(httpCode);
    if (httpCode > 0) {
      String json = http.getString();
      parseForecast(json);
      update_forecast = true;
    }
    http.end();
    digitalWrite(14, LOW);
  }

}

void dhtRead() {
  temperature = sensor.readTemperature();
  humidity = sensor.readHumidity();
}


bool pushToThingSpeak() {
  if (temperature > 0) {

    String url = "/update?key=";
    url += ts_key;
    url += "&field1=";
    url += String(temperature, 1);
    url += "&field2=";
    url += String(humidity, 1);

    String request =
      "GET " + url +
      " HTTP/1.1\r\n" +
      "Host: " + ts_host + "\r\n" +
      "Connection: close\r\n\r\n";

    WiFiClient client;

    if (!client.connect(ts_host, 80)) {
      Serial.println("Error while connecting to ThingSpeak");
      return false;
    }

    client.print(request);
    Serial.println("Push to ThingSpeak OK");
    return true;

  }

  return false;

}

void connectToWiFi() {
  WiFi.begin(ssid, password);
  lcd.print("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    lcd.print('.');
  };
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connected to: ");
  lcd.print(ssid);
  lcd.setCursor(0, 1);
  lcd.print("IP: ");
  lcd.print(WiFi.localIP());

}

void displayCurrentConditions() {
  //Current conditions+DHT Readings

  if (full_update) {
    //Clear screen and print

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Now: ");

    lcd.setCursor(0, 1);
    lcd.print(currentTime);

    lcd.setCursor(0, 3);
    lcd.print(currentWeather);

    //Disable full_update;
    full_update = false;

  }
  lcd.setCursor(0, 2);
  lcd.print(temperature);
  lcd.print("oC ");
  lcd.print(humidity);
  lcd.print("%");
}

void displayForecast() {
  if (update_forecast) {

    lcd.clear();
    //----------------LINE 0----------------
    lcd.setCursor(0, 0);
    lcd.print(forecast_days[dayId].week_day);
    lcd.print(" ");
    if (forecast_days[dayId].date_day < 10) {
      lcd.print('0');
    }
    lcd.print(forecast_days[dayId].date_day);
    lcd.print('.');
    lcd.print(forecast_days[dayId].monthname);
    //----------------LINE 1----------------
    lcd.setCursor(0, 1);
    lcd.print("L:");
    lcd.print(forecast_days[dayId].low_temp);
    lcd.print("C H:");
    lcd.print(forecast_days[dayId].high_temp);
    lcd.print("C P:");
    lcd.print(forecast_days[dayId].qpf_allday);
    lcd.print("mm");
    //----------------LINE 2----------------
    lcd.setCursor(0, 2);
    lcd.print("W:");
    lcd.print(forecast_days[dayId].avewind_speed);
    lcd.print("kph D:");
    lcd.print(forecast_days[dayId].avewind_dir);
    //----------------LINE 3----------------
    lcd.setCursor(0, 3);
    lcd.print(forecast_days[dayId].conditions);
    update_forecast = false;
  };
}


void updateData() {
  getCurrentConditions();
  getForecast();
  pushToThingSpeak();
}

String cutString(String string, int begin, int end) {
  //Cut string to max length 20
  //If string is longer than 20 it will be ended by "."
  if (string.length() > end) {
    string = string.substring(begin, end);
    if (begin == 0)string += ".";
  }
  return string;
}

void setup() {
  Serial.begin(115200);

  pinMode(0, INPUT_PULLUP);
  pinMode(14, OUTPUT);
  digitalWrite(14, LOW);

  lcd.begin(20, 4);
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(0, 0);

  connectToWiFi();
  updateData();
  dhtRead();

  //Setting Timers
  dht.every(dht_interval, dhtRead);
  screen.every(screen_interval, updateScreen);
  data.every(data_interval, updateData);

}

void updateScreen() {
  switch (mode) {
    case 0:
      displayCurrentConditions();
      break;

    case 1:
      displayForecast();
      break;

  }

}

void updatePosition() {
  if (mode == 0) {
    dayId = 1;
    mode = 1;
    update_forecast = true;
  } else if (dayId < 3) {
    dayId++;
    update_forecast = true;
  } else if (dayId == 3) {
    mode = 0;
    dayId = 0;
    full_update = true;
  }

}

void checkIfButtonPressed() {
  if (digitalRead(BUTTONPIN) && (key_released)) {
    delay(10);
    key_released = false;
    updatePosition();
  } else if (!digitalRead(BUTTONPIN)) {
    key_released = true;
  }
}

void loop() {
  dht.update();
  screen.update();
  data.update();
  checkIfButtonPressed();

}
