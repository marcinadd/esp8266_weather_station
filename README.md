# ESP8266 Weather Station

Example sketch of using ESP8266 to acquire data from Weather Underground and push local readings to ThingSpeak.

## Getting Started

Go to releases and download the latest version. You can also clone whole repository.

### Prerequisites
#### 1. Hardware
* ESP8266 based board
* DHT sensor
* One button switch and capacitor for debouncing
* Logic Level Converter
* HD44780 4x20 LCD with I2C converter
#### 2. Libraries
* ESP8266 Libraries
* ArduinoJson Library 5.x.x **(6.x.x beta currently not supported)**
* DHT Library
* LCD I2C Library

### Connections

TODO

## Contributing

If you have and suggestions you can add Issue or make Pull Request. I will review they.

## Authors

* **Marcin Zasuwa** - *Initial work* - [marcinadd](https://github.com/marcinadd)

## License

This project is licensed under the Unlicense - see the [LICENSE.md](LICENSE.md) file for details

